import React from 'react';
import FirstScreen from './src/Screens/FirstScreen';
import ProductDetails from './src/Screens/ProductDetails';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export type RootStackParams = {
  HOME: undefined,
  DETAILS: {selectedID:number}
};

const Stack = createNativeStackNavigator<RootStackParams>();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HOME" screenOptions={{headerShown:false}}>
        <Stack.Screen name="HOME" component={FirstScreen}/>
        <Stack.Screen name="DETAILS" component={ProductDetails}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

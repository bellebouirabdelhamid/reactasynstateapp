import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { makeStyles } from './styles';

type Product = {
  id: number,
  title: string,
  price: number,
  image: string,
  showDetails: () => void,
};

const ProductCard = ({ id, title, price, image, showDetails }: Product) => {
  const styles = makeStyles();
  return (
    <TouchableOpacity
      key={id}
      style={styles.productContainer}
      onPress={showDetails}>
      <View style={styles.productInfos}>
        <Text>Title : {title}</Text>
        <Text>Price : {price} $</Text>
      </View>
      <View style={styles.productPicContainer}>
        <Image style={styles.productpicImage} source={{ uri: image }} />
      </View>
    </TouchableOpacity>
  );
};

export default ProductCard;

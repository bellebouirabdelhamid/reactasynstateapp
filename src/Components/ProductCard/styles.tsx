import { StyleSheet } from 'react-native';

export const makeStyles = () => {
  return StyleSheet.create({
    productContainer: {
      borderColor: 'black',
      borderWidth: 1,
      flexDirection: 'row',
    },
    productInfos: {
      paddingLeft: 10,
      width: '75%',
    },
    productPicContainer: {
      borderColor: 'black',
      borderWidth: 1,
    },
    productpicImage: {
      width: 95,
      height: 95,
    },
  });
};

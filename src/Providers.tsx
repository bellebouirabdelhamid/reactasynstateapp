import React from 'react';
import { AsyncStateProvider } from 'react-async-states';
import { View } from 'react-native';

type AppProviderProps = {
  children: Element,
};

export default function AppProviders({ children }: AppProviderProps) {
  return (
    <View>
      <AsyncStateProvider>{children}</AsyncStateProvider>;
    </View>
  );
}

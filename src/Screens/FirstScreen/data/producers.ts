import axios from 'axios';
const API = axios.create({ baseURL: 'https://fakestoreapi.com' });
const getFromAPI = () => API.get('/products');
export default function GetProductProducer() {
  return getFromAPI().then(({ data }) => data);
}

import React from 'react';
import {
  View,
  Text,
  Button,
  FlatList,
  ListRenderItem,
  ListRenderItemInfo,
} from 'react-native';
import { makeStyles } from './styles';
import GetProductProducer from './data/producers';
import { useAsyncState, UseAsyncState } from 'react-async-states';
import ProductCard from '../../Components/ProductCard';
import { NativeStackScreenProps } from '@react-navigation/native-stack/lib/typescript/src/types';
import { RootStackParams } from '../../../App';

type HomeScreenProps = NativeStackScreenProps<RootStackParams, 'HOME'>;

type Product = {
  id: number,
  title: string,
  price: number,
  description: string,
  image: string,
};

const FirstScreen: React.FC<HomeScreenProps> = ({ navigation }) => {
  const styles = makeStyles();
  const { state, run }: UseAsyncState<Product[]> = useAsyncState({
    producer: GetProductProducer,
    lazy: false,
  });
  const { data } = state;

  const renderProductItems: ListRenderItem<Product> = ({
    item,
  }: ListRenderItemInfo<Product>) => (
    <ProductCard
      id={item.id}
      key={item.id}
      title={item.title}
      price={item.price}
      image={item.image}
      showDetails={() => {
        navigation.push('DETAILS', { selectedID: item.id });
      }}
    />
  );

  return (
    <View>
      <Text style={styles.textStyle}>Welcome !</Text>
      <Button title="Get products" onPress={() => run()} />
      <FlatList
        style={styles.container}
        data={data}
        renderItem={renderProductItems}
      />
    </View>
  );
};

export default FirstScreen;

import { StyleSheet } from 'react-native';

export const makeStyles = () => {
  return StyleSheet.create({
    textStyle: {
      color: 'red',
      fontSize: 50,
      textAlign: 'center',
    },
    container: {
      marginVertical: 10,
      height: '84.5%',
      borderColor: 'black',
      borderWidth: 2,
    },
  });
};

import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { makeStyles } from './styles';
import { useAsyncState } from 'react-async-states';
import { addOrRemoveFromPanelResource } from '../../data/ressources';
import { PanelIcon } from '../../../../assets/icons';
import Modal from '../Modal';

type ProductProps = {
  Title: string,
};

const AddOrRemoveButtons = ({ Title }: ProductProps) => {
  const styles = makeStyles();
  const [isVisible, setfIsVisible] = useState(false);
  const hideModalHandler = () => {
    setfIsVisible(false);
  };
  const {
    state: { data },
    run,
  } = useAsyncState({
    source: addOrRemoveFromPanelResource,
  });
  return (
    <>
      <View style={styles.contorlPanelContianer}>
        <TouchableOpacity
          style={styles.buttonsControlPanel}
          onPress={() => run(1)}>
          <Text style={styles.textButtonsControlPanel}>Add to panel</Text>
        </TouchableOpacity>
        <Text style={styles.numberProductsControlPanel}>Panel : {data}</Text>
        <TouchableOpacity
          style={styles.buttonsControlPanel}
          onPress={() => run(-1)}>
          <Text style={styles.textButtonsControlPanel}>Remove from panel</Text>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity
          style={styles.buyButtonStyle}
          onPress={() => setfIsVisible(true)}>
          <PanelIcon />
          <Text style={styles.textButtonsControlPanel}> Buy</Text>
        </TouchableOpacity>
      </View>
      <Modal
        ProductNumber={data}
        ProductTitle={Title}
        modalIsVisible={isVisible}
        onPressHandler={hideModalHandler}
      />
    </>
  );
};

export default AddOrRemoveButtons;

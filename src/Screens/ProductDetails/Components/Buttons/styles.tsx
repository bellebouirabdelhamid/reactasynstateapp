import { StyleSheet } from 'react-native';

export const makeStyles = () => {
  return StyleSheet.create({
    contorlPanelContianer: {
      flexDirection: 'row',
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
    },
    buttonsControlPanel: {
      borderRadius: 15,
      borderColor: 'black',
      borderWidth: 1,
      padding: 10,
      backgroundColor: '#334A73',
    },
    textButtonsControlPanel: {
      color: 'white',
      fontSize: 15,
      fontWeight: '500',
    },
    numberProductsControlPanel: {
      fontSize: 15,
      fontWeight: '600',
    },
    buyButtonStyle: {
      borderRadius: 15,
      borderColor: 'black',
      borderWidth: 1,
      padding: 10,
      backgroundColor: '#334A73',
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      width: '50%',
      alignSelf: 'center',
      marginTop: 20,
    },
  });
};

import React from 'react';
import { View, Text, Pressable } from 'react-native';
import RNModal from 'react-native-modal';
import { makeStyles } from './styles';

type ModalProps = {
  modalIsVisible: boolean,
  ProductTitle: string,
  ProductNumber: number,
  onPressHandler: () => void,
};

const Modal = ({
  modalIsVisible,
  ProductTitle,
  ProductNumber,
  onPressHandler,
}: ModalProps) => {
  const styles = makeStyles();
  return (
    <RNModal
      isVisible={modalIsVisible}
      animationIn="zoomIn"
      animationOut="zoomOut">
      <View style={styles.modalContainer}>
        <View>
          <Text style={styles.modalTitle}>Confirmation</Text>
          <Text style={styles.modalText}>
            You are going to buy {ProductNumber} of {ProductTitle}
          </Text>
        </View>
        <Pressable onPress={onPressHandler} style={styles.modalButton}>
          <Text style={styles.modalButtonText}>OK</Text>
        </Pressable>
      </View>
    </RNModal>
  );
};

export default Modal;

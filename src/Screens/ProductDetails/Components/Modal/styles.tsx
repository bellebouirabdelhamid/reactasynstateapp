import { StyleSheet } from 'react-native';

export const makeStyles = () => {
  return StyleSheet.create({
    modalContainer: {
      backgroundColor: 'white',
      borderRadius: 8,
      paddingHorizontal: 10,
      paddingVertical: 14,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    modalTitle: {
      fontSize: 18,
      fontWeight: '700',
    },
    modalText: {
      marginVertical: 10,
    },
    modalButton: {
      backgroundColor: '#222f3e',
      paddingVertical: 6,
      paddingHorizontal: 10,
      borderRadius: 4,
      marginLeft: 10,
      elevation: 2,
      shadowColor: '#000',
      shadowOffset: {
        width: 2,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.5,
      alignSelf: 'flex-end',
    },
    modalButtonText: {
      color: 'white',
      fontSize: 16,
    },
  });
};

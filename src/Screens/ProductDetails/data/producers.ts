import axios from 'axios';
import { ProducerProps } from 'react-async-states';

const API = axios.create({ baseURL: 'https://fakestoreapi.com' });
const getFromAPI = (id) => API.get(`/products/${id}`);

export default function getProductDataProducer({ payload }) {
  return getFromAPI(payload.selectedID).then(({ data }) => data);
}

export const addOrRemoveToPanelProducer = ({
  lastSuccess,
  args: [alt],
}: ProducerProps<number>) => {
  const result = lastSuccess.data + Number(alt);
  if (result < 0) {
    return 0;
  }
  return result;
};

import { createSource } from 'react-async-states';
import { addOrRemoveToPanelProducer } from './producers';

export const addOrRemoveFromPanelResource = createSource(
  'product-panel',
  addOrRemoveToPanelProducer,
  { initialValue: 0 },
);

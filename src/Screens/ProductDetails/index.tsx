import React from 'react';
import { useAsyncState } from 'react-async-states';
import { View, Text, Image } from 'react-native';
import getProductDataProducer from './data/producers';
import { NativeStackScreenProps } from '@react-navigation/native-stack/lib/typescript/src/types';
import { RootStackParams } from '../../../App';
import { makeStyles } from './styles';
import AddOrRemoveButtons from './Components/Buttons/AddOrRemoveButtons';

type DetailsScreenProps = NativeStackScreenProps<RootStackParams, 'DETAILS'>;

const ProductDetails: React.FC<DetailsScreenProps> = ({ route }) => {
  const styles = makeStyles();
  const { selectedID } = route.params;
  const { state } = useAsyncState(
    {
      lazy: false,
      producer: getProductDataProducer,
      payload: { selectedID },
    },
    [selectedID],
  );
  const { data } = state;
  return (
    <View key={data?.id} style={styles.container}>
      <>
        <View style={styles.containerOfEachData}>
          <Text style={styles.titleTextStyle}>Title :</Text>
          <Text>{data?.title}</Text>
        </View>
        <View style={styles.containerOfEachData}>
          <Text style={styles.titleTextStyle}>Price :</Text>
          <Text>{data?.price} $</Text>
        </View>
        <View style={styles.containerOfEachData}>
          <Text style={styles.titleTextStyle}>Description :</Text>
          <Text>{data?.description}</Text>
        </View>
      </>
      <>
        <Image
          source={{ uri: data?.image, height: 150, width: 150 }}
          style={styles.imageContainer}
        />
      </>
      <AddOrRemoveButtons Title={data?.title} />
    </View>
  );
};

export default ProductDetails;

import { StyleSheet } from 'react-native';

export const makeStyles = () => {
  return StyleSheet.create({
    container: {
      padding: 10,
    },
    titleTextStyle: {
      fontSize: 20,
    },
    containerOfEachData: {
      marginVertical: 15,
      marginHorizontal: 15,
    },
    imageContainer: {
      alignSelf: 'center',
      borderColor: 'black',
      borderWidth: 1,
      marginVertical: 15,
    },
  });
};

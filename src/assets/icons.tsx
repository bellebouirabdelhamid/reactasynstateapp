import * as React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';

const PanelIcon = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={50}
    height={50}
    viewBox="0 0 24 24"
    fill="none"
    stroke="#FFF"
    strokeWidth={2}
    strokeLinecap="round"
    strokeLinejoin="round"
    {...props}>
    <Circle cx={10} cy={20.5} r={1} />
    <Circle cx={18} cy={20.5} r={1} />
    <Path d="M2.5 2.5h3l2.7 12.4a2 2 0 0 0 2 1.6h7.7a2 2 0 0 0 2-1.6l1.6-8.4H7.1" />
  </Svg>
);

export { PanelIcon };

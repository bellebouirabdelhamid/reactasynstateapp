import { AsyncStateStatus } from 'react-async-states';

export const extendStatus = ({ status }) => ({
  isPending: status === AsyncStateStatus.pending,
  isError: status === AsyncStateStatus.error,
  isSuccess: status === AsyncStateStatus.success,
});

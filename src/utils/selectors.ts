import { extendStatus } from './dataFormatters';

export default function defaultSelector(state) {
  return {
    data: state.data,
    ...extendStatus(state),
  };
}
